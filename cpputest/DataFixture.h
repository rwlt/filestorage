#ifndef D_DATAFIXTURE_H
#define D_DATAFIXTURE_H

#include "storage/RowRecord.h"
#include "storage/VersionRecord.h"
#include <memory>

///////////////////////////////////////////////////////////////////////////////
//
//  FileFixture:
//
///////////////////////////////////////////////////////////////////////////////

class DataFixture
{
public:
    explicit DataFixture():
    test("123456789012345678901234567890w"),
    test2("abcdefghijklmnopqrstuvwxyz"),
    test3("DDCCFFGGNNRRJJSSLLSLLEEEDF"),
    id(105510), id2(205510), id3(305510),
    desc( "Module File 1.0 - July 16, 2016" ),
    versionRec(desc.c_str(), 2),
    progRec(id, test.c_str()),
    progRec2(id2, test2.c_str()),
    progRec3(id3, test3.c_str())
    {
    };
    virtual ~DataFixture() {};
    std::string test, test2, test3;
    int id, id2, id3;
    std::string desc;
    Storage::VersionRecord versionRec;
    Storage::RowRecord progRec;
    Storage::RowRecord progRec2;
    Storage::RowRecord progRec3;
    
private:
    
    DataFixture ( const DataFixture & );
    DataFixture &operator= ( const DataFixture & );
    
};

#endif  // D_DATAFIXTURE_H
