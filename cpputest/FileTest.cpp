#include "DataFixture.h"
#include "storage/RowRecord.h"
#include <iostream>
#include <cstring>

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
//
//-------------------------------------------------------
// ControllerAccessor test group
//-------------------------------------------------------
TEST_GROUP(FileAccessor)
{
    DataFixture fixture;

};

TEST(FileAccessor, RowRecordAccessor)
{
    std::string test;
    std::string test2;
    test = "123456789012345678901234567890w";
    Storage::RowRecord progRec(10, test.c_str());
    //std::cout << "Length:" << strlen(progRec.getField1().c_str()) << '\n';
    CHECK(strlen(progRec.getField1().c_str()) == 31);
    STRCMP_EQUAL(test.c_str(), progRec.getField1().c_str());

    test = "1234567890123456789012345678901234567890w";
    test2 = "1234567890123456789012345678901234567890";
    Storage::RowRecord progRec2(10, test.c_str());
    //std::cout << "Length:" << strlen(progRec2.getField1().c_str()) << '\n';
    CHECK(strlen(progRec2.getField1().c_str()) == 40);
    STRCMP_EQUAL(test2.c_str(), progRec2.getField1().c_str());

    std::string test3(progRec2.getField1().c_str());
    CHECK(test3 == test2);

}

TEST(FileAccessor, WriteReadFileAccessor)
{
    std::ofstream ofs;
    ofs.open("filedata.bin", std::ios_base::binary | std::ios::out);
    CHECK(ofs.fail() == false);
    CHECK(ofs.good() == true);

    if (ofs.good())
        fixture.versionRec.StreamWrite(ofs);
    if (ofs.good())
        fixture.progRec.StreamWrite(ofs);
    if (ofs.good())
        fixture.progRec2.StreamWrite(ofs);
    if (ofs.good())
        fixture.progRec3.StreamWrite(ofs);
    ofs.close();

    CHECK(ofs.fail() == false);
    CHECK(ofs.good() == true);

    Storage::VersionRecord versionRead;
    Storage::RowRecord progRead;
    Storage::RowRecord progRead2;
    Storage::RowRecord progRead3;
    std::ifstream ifstest("filedata.in", std::ios_base::binary | std::ios::in);
    CHECK(ifstest.fail() == true);
    CHECK(ifstest.good() == false);
    ifstest.close();
    
    std::ifstream ifs;
    ifs.open("filedata.bin", std::ios_base::binary | std::ios::in);
    if (ifs.good())
        versionRead.StreamRead(ifs);
    if (ifs.good())
        progRead.StreamRead(ifs);
    if (ifs.good())
        progRead2.StreamRead(ifs);
    if (ifs.good())
        progRead3.StreamRead(ifs);
    STRCMP_EQUAL(fixture.versionRec.getDescription().c_str(), versionRead.getDescription().c_str());
    STRCMP_EQUAL(fixture.progRec.getField1().c_str(), progRead.getField1().c_str());
    STRCMP_EQUAL(fixture.progRec2.getField1().c_str(), progRead2.getField1().c_str());
    STRCMP_EQUAL(fixture.progRec3.getField1().c_str(), progRead3.getField1().c_str());
    LONGS_EQUAL(fixture.progRec.getRecordID(), progRead.getRecordID());
    LONGS_EQUAL(fixture.progRec2.getRecordID(), progRead2.getRecordID());
    LONGS_EQUAL(fixture.progRec3.getRecordID(), progRead3.getRecordID());
    ifs.close();
    CHECK(ifs.good() == true);
    
}
