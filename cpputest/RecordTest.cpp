
#include "storage/RowRecord.h"
#include <iostream>
#include <cstring>
#include <vector>

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"


//TEST_GROUP(RowRecordAccessor)
//{
//    std::vector<Storage::RowRecord> rowRec1;
//    std::vector<Storage::RowRecord> rowRec2;
//    std::vector<Storage::RowRecord> rowRecRead1;
//    std::vector<Storage::RowRecord> rowRecRead2;
//
//	void setup ( ) {
//		// Write row records setup
//        //std::vector<Storage::RowRecord>::iterator row_iter;
//        int index = 0;
//        while (index < 3){
//            rowRec1.push_back(Storage::RowRecord());
//            ++index;
//        }
//        index = 0;
//        while (index < 4){
//            rowRec2.push_back(Storage::RowRecord());
//            ++index;
//        }
//        // Read records setup
//        index = 0;
//        while (index < 3){
//        	rowRecRead1.push_back(Storage::RowRecord());
//            ++index;
//        }
//        index = 0;
//        while (index < 4){
//        	rowRecRead2.push_back(Storage::RowRecord());
//            ++index;
//        }
//
//        // Now put in some test data in row records to write
//        index = 10;// temp ids
//        for (std::vector<Storage::RowRecord>::iterator row_iter = rowRec1.begin(); row_iter != rowRec1.end(); ++row_iter){
//            row_iter->setRecordID(index);
//            row_iter->setField1("Blahla la blah");
//            row_iter->setField2("Blahla la blah");
//            row_iter->setField3("Blahla la blah");
//            index = index + 10;
//        }
//        for (std::vector<Storage::RowRecord>::iterator row_iter = rowRec2.begin(); row_iter != rowRec2.end(); ++row_iter){
//            row_iter->setRecordID(index);
//            row_iter->setField1("Blah blah");
//            index = index + 10;
//        }
//
//
//	}
//};
//
//TEST(RowRecordAccessor, StreamReadAndWriteAccessors)
//{
//    std::vector<Storage::RowRecord>::iterator row_iter;
//
//    std::ofstream ofs;
//    ofs.open("rowdata.bin", std::ios_base::binary | std::ios::out);
//    CHECK(ofs.good() == true);
//    for (row_iter = rowRec1.begin(); row_iter != rowRec1.end(); ++row_iter){
//        if (ofs.good())
//            row_iter->StreamWrite(ofs);
//    }
//    for (row_iter = rowRec2.begin(); row_iter != rowRec2.end(); ++row_iter){
//        if (ofs.good())
//            row_iter->StreamWrite(ofs);
//    }
//    ofs.close();
//    CHECK(ofs.good() == true);
//
//    std::ifstream ifs;
//    ifs.open("rowdata.bin", std::ios_base::binary | std::ios::in);
//    for (row_iter = rowRecRead1.begin(); row_iter != rowRecRead1.end(); ++row_iter){
//        if (ifs.good())
//            row_iter->StreamRead(ifs);
//    }
//    for (row_iter = rowRecRead2.begin(); row_iter != rowRecRead2.end(); ++row_iter){
//        if (ifs.good())
//            row_iter->StreamRead(ifs);
//    }
//    ifs.close();
//    CHECK(ifs.good() == true);
//
//    LONGS_EQUAL(rowRec1.size(), rowRecRead1.size());
//    LONGS_EQUAL(rowRec2.size(), rowRecRead2.size());
//    int index = 0;
//    for (row_iter = rowRec1.begin(); row_iter != rowRec1.end(); ++row_iter){
//        LONGS_EQUAL(row_iter->getRecordID(), rowRecRead1.at(index).getRecordID());
//        STRCMP_EQUAL(row_iter->getField1().c_str(), rowRecRead1.at(index).getField1().c_str());
//        STRCMP_EQUAL(row_iter->getField2().c_str(), rowRecRead1.at(index).getField2().c_str());
//        STRCMP_EQUAL(row_iter->getField3().c_str(), rowRecRead1.at(index).getField3().c_str());
//        ++index;
//    }
//    index = 0;
//    for (row_iter = rowRec2.begin(); row_iter != rowRec2.end(); ++row_iter){
//        LONGS_EQUAL(row_iter->getRecordID(), rowRecRead2.at(index).getRecordID());
//        STRCMP_EQUAL(row_iter->getField1().c_str(), rowRecRead2.at(index).getField1().c_str());
//        ++index;
//    }
//
//}
