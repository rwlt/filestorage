/*
 * TableRecordTest.cpp
 */

#include "storage/TableRecord.h"

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

////
////-------------------------------------------------------
//// TableRecordAccessor test group
////-------------------------------------------------------
//TEST_GROUP(TableRecordAccessor)
//{
//
//};
//
//
//TEST(TableRecordAccessor, StreamReadAndWriteAccessors)
//{
//    Storage::TableRecord tableRec1{"Table1", 1, 3};
//    Storage::TableRecord tableRec2{"Table2", 1, 4};
//    Storage::TableRecord tableRead1;
//    Storage::TableRecord tableRead2;
//
//    std::ofstream ofs;
//    ofs.open("tabledata.bin", std::ios_base::binary | std::ios::out);
//    CHECK(ofs.good() == true);
//    if (ofs.good())
//        tableRec1.StreamWrite(ofs);
//    if (ofs.good())
//        tableRec2.StreamWrite(ofs);
//    ofs.close();
//    CHECK(ofs.good() == true);
//
//    std::ifstream ifs;
//    ifs.open("tabledata.bin", std::ios_base::binary | std::ios::in);
//    if (ifs.good())
//        tableRead1.StreamRead(ifs);
//    if (ifs.good())
//        tableRead2.StreamRead(ifs);
//    ifs.close();
//    CHECK(ifs.good() == true);
//
//    STRCMP_EQUAL(tableRec1.getTablename().c_str(), tableRead1.getTablename().c_str());
//    LONGS_EQUAL(tableRec1.getTotalColumns(), tableRead1.getTotalColumns());
//    LONGS_EQUAL(tableRec1.getTotalRows(), tableRead1.getTotalRows());
//
//    STRCMP_EQUAL(tableRec2.getTablename().c_str(), tableRead2.getTablename().c_str());
//    LONGS_EQUAL(tableRec2.getTotalColumns(), tableRead2.getTotalColumns());
//    LONGS_EQUAL(tableRec2.getTotalRows(), tableRead2.getTotalRows());
//
//}







