/*
 * VersionRecordTest.cpp
 *
 *  Created on: 3/06/2017
 *      Author: rodney
 */
#include "storage/VersionRecord.h"
#include "DerivedBind.h"

#include <iostream>
#include <cstring>

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

////
////-------------------------------------------------------
//// VersionRecordAccessor test group
////-------------------------------------------------------
//TEST_GROUP(VersionRecordAccessor)
//{
//    std::string desc{"Module File 1.0 - July 16, 2016"};
//    void setup() {
//
//    }
//
//};
//
//TEST(VersionRecordAccessor, StreamReadAndWriteAccessors)
//{
//    Storage::VersionRecord versionRec{desc.c_str(), 2};
//
//    std::ofstream ofs;
//    ofs.open("versiondata.bin", std::ios_base::binary | std::ios::out);
//    CHECK(ofs.good() == true);
//    if (ofs.good())
//        versionRec.StreamWrite(ofs);
//    ofs.close();
//    CHECK(ofs.good() == true);
//
//    Storage::VersionRecord versionRead;
//    std::ifstream ifs;
//    ifs.open("versiondata.bin", std::ios_base::binary | std::ios::in);
//    if (ifs.good())
//        versionRead.StreamRead(ifs);
//    ifs.close();
//
//    CHECK(ifs.good() == true);
//    STRCMP_EQUAL(versionRec.getDescription().c_str(), versionRead.getDescription().c_str());
//    LONGS_EQUAL(versionRec.getTotalTables(), versionRead.getTotalTables());
//
//}





