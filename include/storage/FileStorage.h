#ifndef FILESTORAGE_H_
#define FILESTORAGE_H_

#include "RowRecord.h"
#include <unordered_map>
#include <vector>
#include <string>

namespace Storage
{


/**
 *
 * \page filestorage File Storage Module
 *
 * An implementation of storing data tables in a file.
 *
 * \sa Module::CommandControler()
 */
/// Tables stored uses a destination bind source and a Bind for binary file storage
/**
 * \sa Storage::FileStorage,  Control::Bind, Control::BindSourceBase and Storage::RowRecord
 */
struct StoreTable {
//    Storage::Bind<Storage::RowRecord>* bind; ///< bind for toBind and doUpdate from source
   // Control::BindBase* destSource;     ///< uses a BindSource to store the data from file
    int columns;                         ///< the number of columns in each row
    int size;                            ///< the number of items in table
};

/// Table map for StoreTable information
typedef std::unordered_map<std::string, StoreTable> TableMap;
/// Iterator for map of table names
typedef std::unordered_map<std::string, StoreTable>::iterator TableMap_Iter;

/// Knows how to read and write a binary file for a set of tables with BindSource items
class FileStorage
{
public:
    /// Create with a storage version so we can use the same files to the version
    FileStorage ( std::string version );
    /// Virtual deconstructor
    virtual ~FileStorage();
    /// Add a table to store with the binary file
    /**
     * \param tableName string of the table name
     * \param bind the bind to doBind and doUpdate to the BindSource
     * \param columns int of the number of columns in this table stored in the file
     * \param size int of the number of items this table stored in the file
     */
    // void storeTable ( const std::string& tableName, Storage::Bind<Storage::RowRecord>* bind,
    //                  // Control::BindBase* destSource,
    // 		int columns, int size );
    /// Get the total tables in the file storage
    int totalTables();
    /// Get the storage description
    void doReadVersion ( std::string filename );
    /// Read the file storage filename
    /**
     * \param filename string of the file name where the binary data is in
     */
    void doReadStore ( std::string filename );
    /// Write the file storage to the filename
    /**
     * \param filename string of the file name where the binary data is created
     */
    void doUpdateStore ( std::string filename );
    /// Get the BindSource what the file storage is using (different one form tables dest BindSources)
    /**
     * \return pointer to bind source of file storage of row records
     */
    std::vector<Storage::RowRecord>* getStoreSource() {
        return &storeSource;
    };
    /// Unable to read the binary file
    /**
     * \return if unable to read return true
     */
    bool unableRead() {
        return m_unableRead;
    };

private:
//    FileStorage ( const FileStorage & );
//    FileStorage &operator= ( const FileStorage & );

    TableMap mapTableSource;
    std::vector<Storage::RowRecord> storeSource;
    std::vector<Storage::RowRecord>::iterator storeSource_iter;
    std::string m_version;
    bool m_unableRead;
    std::vector<std::string> sortedTablenames;
    std::vector<std::string>::iterator sortedTablenames_iter;
    void sortTablenames();

};

} /* namespace Storage */

#endif /* FILESTORAGE_H_ */
