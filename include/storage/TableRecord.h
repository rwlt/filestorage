#ifndef D_TABLERECORD_H
#define D_TABLERECORD_H

#include "FileRecord.h"

/// For file data sources
namespace Storage
{

/// Knows how to write data to a binary file
/********************************************************//**
 * This shows information of the column and rows count.
 * Needed for when reading and writing to a binary so it is
 * keeping a static copy of the tables data.
 ************************************************************/
class TableRecord: public FileRecord
{
public:
    /// Creates an empy table record so it can be set up. 
    TableRecord() {  };
    /// Can create a new table record with the minimum details.
    /**
     * \param tablename string argument of table name
     * \param totalcolumns int of columns 
     * \param totalrows int of total rows
     * 
     */
    TableRecord ( const char *tablename, int totalcolumns, int totalrows ) :
        m_totalColumns ( totalcolumns ),
        m_totalRows ( totalrows ) {
        m_tablename[0] = '\0';
        setField ( m_tablename, tablename, sizeof ( m_tablename ) );
    };
    virtual ~TableRecord() {
    };

    /// Knows how to write to file the table records members
    void StreamWrite ( std::ofstream &ofs ) {
        ofs.write ( ( char * ) &m_tablename[0], sizeof ( m_tablename ) );
        binary_write ( ofs, m_totalColumns );
        binary_write ( ofs, m_totalRows );
    };
    /// Knows how to read from file the table records members
    void StreamRead ( std::ifstream &ifs ) {
        //char read[sizeof ( m_description )];
        ifs.read ( ( char * ) &m_tablename[0], sizeof ( m_tablename ) );
        binary_read ( ifs, m_totalColumns );
        binary_read ( ifs, m_totalRows );
        // Really set field
        //setField(m_description, read, sizeof(m_description));
    };
    /// The table name
    std::string getTablename() const {
        return std::string ( m_tablename );
    };
    /// The total rows
    int getTotalRows() {
        return m_totalRows;
    };
    /// The total columns
    int getTotalColumns() {
        return m_totalColumns;
    };

private:
    char m_tablename[41]; // 40 charactores allowed plus null
    int m_totalColumns{0};
    int m_totalRows{0};

};

}
#endif  /* D_TABLERECORD_H */
