#ifndef D_VERSIONRECORD_H
#define D_VERSIONRECORD_H

#include "FileRecord.h"
#include <fstream>
using namespace std;

namespace Storage
{

/// Version information of the storage details so we can read and write the same files
class VersionRecord: public FileRecord
{
public:
    /// Empty version
    VersionRecord() {  };
    /// Minimum information for version details
    /**
     * \param desc string description of the version information
     * \param totalTables int of the total tables in the storage binary file
     */
    VersionRecord(const char *desc, int totalTables): m_totalTables( totalTables ) {
        m_description[0] = '\0';
        setField(m_description, desc, sizeof(m_description));
    };
    /// Virtual deconstructor
    virtual ~VersionRecord() {
    };
    /// Knows how to write a version record
    void StreamWrite ( ofstream &ofs ) {
        ofs.write ( ( char * ) &m_description[0], sizeof ( m_description ) );
        binary_write ( ofs, m_totalTables );
    };
    /// Knows how to read a version record
    void StreamRead ( ifstream &ifs ) {
        //char read[sizeof ( m_description )];
        ifs.read ( ( char * ) &m_description[0], sizeof ( m_description ) );
        binary_read ( ifs, m_totalTables );
        // Really set field
        //setField(m_description, read, sizeof(m_description));
    };
    /// Get the version desription
    /**
     * \return string of the version description
     */
    std::string getDescription() {
        return std::string ( m_description );
    };
    /// Get the number of tables
    /**
     * \return int ot total tables
     */
    int getTotalTables() {
        return m_totalTables;
    };

private:
    char m_description[41]; // 40 charactores allowed plus null
    int m_totalTables{0};
    
};

}
#endif  /* D_VERSIONRECORD_H */
