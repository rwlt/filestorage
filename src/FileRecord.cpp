/*
 * FileRecord.cpp
 */

#include "storage/FileRecord.h"
#include <iostream>
#include <cstring>

namespace Storage
{

//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
FileRecord::FileRecord()
{
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
FileRecord::~FileRecord()
{
}
//
//-------------------------------------------------------
// Private method
//-------------------------------------------------------
//
void FileRecord::setField(char *field, const char *value, int size)
{
    //std::cout << "Size: " << size << '\n';
    int length = strnlen(value, size);
    if (length == size) {
        length--;    // make sure room for null terminator
    }
    if (length > 0) {
        strncpy(field, value, length);
        field[length] = '\0';
    }
}


} /* namespace Storage */
