/*
 * FileStorage.cpp
 */

#include "storage/FileStorage.h"
#include "storage/VersionRecord.h"
#include "storage/TableRecord.h"
#include <iostream>
#include <sstream>
#include <algorithm>

namespace Storage
{
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
FileStorage::FileStorage(std::string version): m_version(version), m_unableRead(false)
{

}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
FileStorage::~FileStorage()
{

}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
// void FileStorage::storeTable(const std::string &tableName, Storage::Bind<Storage::RowRecord> *bind,
//                             /* Control::BindBase *destSource,*/ int columns, int size)
// {
//     StoreTable tableStore {bind, columns, size};
//     //StoreTable tableStore {bind, destSource, columns};
//     // Makes non duplicated keys on tableName
//     mapTableSource.insert(std::make_pair(tableName, tableStore));
// }
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void FileStorage::doUpdateStore(std::string filename)
{
    StoreTable tableStore;
    VersionRecord versionRec(m_version.c_str(), mapTableSource.size());
    
    // VersionRecord
    std::ofstream ofs;
    ofs.open(filename.c_str(), std::ios_base::binary | std::ios::out);
    
    if (ofs.good())
        versionRec.StreamWrite(ofs);

    sortTablenames();    // sort mapTableSource keys

    // TableRecords
    // For each table now write TableRecord to file
    // Sorted by ascending order
    for (sortedTablenames_iter = sortedTablenames.begin(); sortedTablenames_iter != sortedTablenames.end(); ++sortedTablenames_iter) {
        std::string tableName = *sortedTablenames_iter;
        // Get the tables TableStore setting for destSource and Bind's
        tableStore = mapTableSource.at(tableName);
        TableRecord tableRec(tableName.c_str(), tableStore.columns, tableStore.size);
        if (ofs.good())
            tableRec.StreamWrite(ofs);
    }

    // RowRecords
    for (sortedTablenames_iter = sortedTablenames.begin(); sortedTablenames_iter != sortedTablenames.end(); ++sortedTablenames_iter) {
        std::string tableName = *sortedTablenames_iter;
        // Get the tables TableStore setting for destSource and Bind's
        tableStore = mapTableSource.at(tableName);

        // How many to write is from destsource to update from
        // We know how to read the size of the dest source only
        // Then create a new RowRecord and add it to the FileStorage store source
        int size = tableStore.size;
        storeSource.clear(); // Cleanup file storage source now
        for (int index = 0; index < size; ++index) {
            storeSource.push_back(RowRecord());
//	        tableStore.bind->doUpdate(storeSource.at(index), index);
        }
        for (storeSource_iter = storeSource.begin(); storeSource_iter != storeSource.end(); ++ storeSource_iter) {
            if (ofs.good())
                storeSource_iter->StreamWrite(ofs);
        }
        
        //storeSource.removeBind(tableStore.bind); // we dont let binds run anymore
    }
    std::string endoffile("\n\n\n\n");
    ofs.write((char *) endoffile.c_str(), 4);
    storeSource.clear(); // Cleanup file storage source now
    
    ofs.close();
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
// doReadStore - Reads version record and checks if the same as local member m_version
//               If not m_unableRead is true and no reading of file done.
// Parameter
//    filename: filename to read data from
///////////////////////////////////////////////////////////////////////////////
//
void FileStorage::doReadVersion(std::string filename)
{
    // Check VersionRecord
    m_unableRead = false; //reset to reread
    Storage::VersionRecord versionRead;
    std::ifstream ifs;
    ifs.open(filename.c_str(), std::ios_base::binary | std::ios::in);
    if (ifs.good())
        versionRead.StreamRead(ifs);

    if (!(versionRead.getDescription() == m_version && versionRead.getTotalTables() == (int)mapTableSource.size())) {
        m_unableRead = true;
        ifs.close();
        return;
    }
    ifs.close();
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void FileStorage::doReadStore(std::string filename)
{
    TableMap_Iter tableMap_itr;
    StoreTable tableStore;
    Storage::VersionRecord versionRead;
    std::vector<TableRecord> tableRead;
    std::vector<TableRecord>::iterator tableRead_iter;
    std::ifstream ifs;
    std::vector<Storage::RowRecord> rowRec1;
    std::vector<Storage::RowRecord>::iterator row_iter;
    
    doReadVersion(filename);
    if (m_unableRead)
        return;


    ifs.open(filename.c_str(), std::ios_base::binary | std::ios::in);
    if (ifs.good())
        versionRead.StreamRead(ifs);

    for (int index = 0; index < versionRead.getTotalTables(); ++index) {
        tableRead.push_back(TableRecord());
    }
    for (tableRead_iter = tableRead.begin(); tableRead_iter != tableRead.end(); ++tableRead_iter) {
        if (ifs.good())
            tableRead_iter->StreamRead(ifs);
    }
    // Check mapTableSource
    // The order is important for tables - the order made is from the mapTableSource.
    // when written, so when reading and rewriting the initial order written is required to be done
    // on the file.
    // Check the order of mapTableSource items is the same as what is read, set m_unableRead true
    // if different as it will be error in reading the wrong set of tableRecords later.
    for (tableRead_iter = tableRead.begin(); tableRead_iter != tableRead.end(); ++tableRead_iter) {
        tableMap_itr = mapTableSource.find(tableRead_iter->getTablename());
        // Find a read table record tablename - if not unable to read is true
        if (tableMap_itr == mapTableSource.end()) {
            m_unableRead = true;
            break;
        }
        // Check number of columns
        if (tableRead_iter->getTotalColumns() != tableMap_itr->second.columns) {
            m_unableRead = true;
            break;
        }
    }
    if (m_unableRead) {
        ifs.close();
        return;
    }

    // The order of read is always tablename ascending the same as write tables
    sortTablenames();    // sort mapTableSource keys
    
    // For each table now read each tables records.
    for (sortedTablenames_iter = sortedTablenames.begin(); sortedTablenames_iter != sortedTablenames.end(); ++sortedTablenames_iter) {

        std::string tableName = *sortedTablenames_iter; //Used in find_if lambda closure
        // Get the tables TableStore setting for destSource and Bind's
        tableStore = mapTableSource.at(tableName);

        rowRec1.clear(); // TODO:clearing vector not a new one
        
        //We do create empty RowRecord to the size needed found from TableRecord
        tableRead_iter = std::find_if(tableRead.begin(), tableRead.end(), [tableName](const TableRecord & t) -> bool { return t.getTablename() == tableName; });
        int size = tableRead_iter->getTotalRows();
        
        int index = 0;
        while (index < size) {
            rowRec1.push_back(Storage::RowRecord());
            ++index;
        }

        for (row_iter = rowRec1.begin(); row_iter != rowRec1.end(); ++row_iter) {
            if (ifs.good())
                row_iter->StreamRead(ifs);
            //tableStore.bind->doBind(*row_iter);
        }
    }
    ifs.close();
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
int FileStorage::totalTables()
{
    return mapTableSource.size();
}
//
///////////////////////////////////////////////////////////////////////////////
// private function
///////////////////////////////////////////////////////////////////////////////
//
void FileStorage::sortTablenames()
{
    sortedTablenames.clear();
    TableMap_Iter tableMap_itr;
    for (tableMap_itr = mapTableSource.begin(); tableMap_itr != mapTableSource.end(); ++tableMap_itr) {
        sortedTablenames.push_back(tableMap_itr->first);
    }
    std::sort(sortedTablenames.begin(), sortedTablenames.end(), [](const std::string & a, const std::string & b) -> bool { return a < b; });

}
}  /* namespace Storage */
