/*
 * RowRecord.cpp
 */

#include "storage/FileRecord.h"
#include "storage/RowRecord.h"
#include <iostream>
#include <cstring>

namespace Storage
{

//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
RowRecord::RowRecord()
{
    m_recordID = 0;
    //m_Field1[0] = '\0';
   // m_Field2[0] = '\0';
   // m_Field3[0] = '\0';
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
RowRecord::RowRecord(int recordID, const char *field1)
{
    m_recordID = recordID;
    setField(m_Field1, field1, sizeof(m_Field1));
   // m_Field2[0] = '\0';
   // m_Field3[0] = '\0';
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
RowRecord::~RowRecord()
{
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
void RowRecord::StreamWrite(std::ofstream &ofs)
{
    binary_write(ofs, m_recordID);
    //ofs.write((char *) &m_recordID, sizeof(m_recordID));
    ofs.write((char *) &m_Field1[0], sizeof(m_Field1));
    ofs.write((char *) &m_Field2[0], sizeof(m_Field2));
    ofs.write((char *) &m_Field3[0], sizeof(m_Field3));
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
void RowRecord::StreamRead(std::ifstream &ifs)
{
    binary_read(ifs, m_recordID);
    //ifs.read((char *) &m_recordID, sizeof(m_recordID));
    ifs.read((char *) &m_Field1[0], sizeof(m_Field1));
    ifs.read((char *) &m_Field2[0], sizeof(m_Field2));
    ifs.read((char *) &m_Field3[0], sizeof(m_Field3));
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
void RowRecord::setRecordID(int id)
{
    m_recordID = id;
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
int RowRecord::getRecordID()
{
    return m_recordID;
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
void RowRecord::setField1(std::string value)
{
    setField(m_Field1, value.c_str(), sizeof(m_Field1));
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
std::string RowRecord::getField1() const
{
    return std::string(m_Field1);
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
std::string RowRecord::getField2() const
{
    return std::string(m_Field2);
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
void RowRecord::setField2(std::string value)
{
    setField(m_Field2, value.c_str(), sizeof(m_Field2));
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
std::string RowRecord::getField3() const
{
    return std::string(m_Field3);
}
//
//-------------------------------------------------------
// Public method
//-------------------------------------------------------
//
void RowRecord::setField3(std::string value)
{
    setField(m_Field3, value.c_str(), sizeof(m_Field3));
}


} /* namespace Storage */
